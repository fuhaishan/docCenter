## 一、单条上传文件

### 1、api描述
单条上传文件

### 2、api参数

- 请求路径： /api/generalOperatingFile/uploadFiles
- Method: POST

### 3、输入参数

参数名称|是否必选|数据类型|描述
-|-|-|-
file|Y|File|文件对象
### 4、返回参数

- 返回restful标准singlePicResultEntity数据实体

参数名称|是否必选|数据类型|描述
-|-|-|-
fileId|Y|String|文件路径
### 5、请求示例

```
curl -i 
```
```
{
"file":文件,
}

```
### 6、返回示例

```json
{
    "fileName": "tmp15549631101835266558493340269561.jpg",
    "appCode": "nwd-ph-war",
    "fileId": "ndfs3/M09/95/B5/rBMMFlyu2rKAL2JXAADXPI_8lYk682.jpg",
    "ret": 0,
    "dbret": 0
}
```