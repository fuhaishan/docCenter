# 部落OMS系统API错误代码清单


http状态码 | 错误码 | 描述
---|---|---
400 |  | BAD_REQUEST
 -|40001|-新密码或老密码为空
 -|40002|-新密码与老密码不匹配
 -|40003|-请求实体为空，param
 -|40004|-请求参数为空，param=xxxx
 -|40005|-请求参数不在范围内，param=xxxx
 -|40006|-没有与请求参数相匹配的数据，param
 -|40007|-没有发现上传的文件
 -|-|-
 -|400201|-短信验证码不匹配
 -|40011|-短信验证码不匹配
 -|40011|-短信验证码不匹配
 -|40011|-短信验证码不匹配

401 |  |没有提供认证信息
 -|-|-
404 |  | NOT_FOUND 
 -|40401|没有与密码匹配的用户
 -|40402|没有与用户名匹配的用户
 -|40403|没有匹配的用户
 -|40410|没有匹配的部落
 -|40410|没有匹配的部落
 -|404301|没有匹配的动态
409 |  |
-|40901|用户名或手机号码已存在
-|40902|项目已存在主管者
-|-|-
-|409101|-已有相同名称的部落存在

500 |  |INTERNAL_SERVER_ERROR 
-| 50001|未知服务器错误，详见错误提示
-| 50011|验证码发送失败
-| 50021|update执行失败，修改记录为0
