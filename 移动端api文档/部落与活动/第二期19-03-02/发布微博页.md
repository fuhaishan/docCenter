# 部落首页

## 一、发微博接口 (邓成江)

### 1、api描述
获取太阳码接口

### 2、api参数

- 请求路径： api/v20190319/microblog/releaseMicroblog
- Method: POST

### 3、输入参数

参数名称|是否必选|数据类型|描述
-|-|-|-
mid|Y|string|用户id
microblogContent|Y|string|微博类容不能为空
tribeId|N|string|部落id
tribeActivityId|N|string|活动id 
microblogImageList|N|string|微博图片路径集合，数组，可以为空，如果是有发图片的微博则需要传

### 4、返回参数
参数名称|是否必选|数据类型|描述
-|-|-|-
code|Y|Integer|状态码
200|Y|Integer|成功

### 5、请求示例

```
curl -X POST \
  http://localhost:8080/api/v20190319/microblog/releaseMicroblog
  {
    "mid": "420301066000165",
    "microblogContent": "呵呵呵",
    "microblogImageList":[
            "http://www.baidu.com/image/xxx.jpg",
            "http://www.baidu.com/image/xxx.jpg"
      ]
  }
```

### 6、返回示例

```json
{
    "code": "200",
    "msg": "成功",
    "result": "success"
}
```
## 4、动态分享(江磊磊)

### 1、api描述
  用户分享动态时,将该条动态分享次数 + 1

### 2、api参数

- 请求路径：api/v20190319/microblog/microblogShare
- Method: POST

### 3、输入参数

参数名称|是否必选|数据类型|描述
-|-|-|-
microblogId|Y|string|动态ID

### 4、返回参数
参数名称|是否必选|数据类型|描述
-|-|-|-
code|Y|string|状态码
msg|Y|string|状态描述

### 5、请求示例

```
curl -X POST 
  http://localhost:8080/api/microblog/microblogShare
  {
	"microblogId": "111"
}

```

### 6、返回示例

```json
{
  "code": "200",
  "msg": "成功",
  "result": "success"
}
```