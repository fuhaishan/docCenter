
#更多活动页

## 、一、更具活动id活动奖品图

### 1、api描述
 更具活动id活动奖品图

### 2、api参数

- 请求路径：api/v20190319/tribe/findTribeActivityList
- Method: post

### 3、输入参数

参数名称|是否必选|数据类型|描述
-|-|-|-
current|Y|int|当前页码数
tribeInfoId|N|string|部落id（如果是部落下的活动必须要传值）
currentIndex|Y|int|当前页码数
pageSize|Y|int|每次返回条数
activityType|N|int|1:发表评论活动;2:话题活动
### 4、返回参数
参数名称|是否必选|数据类型|描述
-|-|-|-
tribeActivityId|Y|String|活动id
tribeActivityName|Y|String|活动名称
activityDesc|Y|String| 活动内容描述
mainImageUrl|Y|long| 活动主图url
userCount|Y|long|用户参与数
showUserCount|Y|long|注水用户参与数
tribeActivityPrizeImage|Y|String|奖品图
activityType|Y|int|1:发表评论活动;2:话题活动
tribeActivityPrizeName|Y|String|奖品名称

current|Y|int|当前页码数
size|Y|int|每次返回条数   
total|Y|int|总数量
pages|Y|int|总页码数
### 5、请求示例

```
curl 
```

### 6、返回示例

```json
{
    "code": "200",
    "msg": "成功",
    "result": {
        "records": [
            {
                "tribeActivityId": "5",
                "tribeActivityName": "1",
                "activityDesc": "hhhhHhshchac",
                "mainImageUrl": "http://ndfs3.niwodai.net/ndfs3/M04/DA/2C/rBMMF1w39QCALgjyAACSa1XiPPo601.jpg",
                "userCount": 12,
                "showUserCount": 1261111,
                "activityType":1,
                "tribeActivityPrizeImage":"http://ndfs3.niwodai.net/ndfs3/M04/DA/2C/rBMMF1w39QCALgjyAACSa1XiPPo601.jpg"
            },   
        ],
        "total": 21,
        "size": 15,
        "current": 2,
        "searchCount": true,
        "pages": 2
    }
```


#更多活动页

## 、一、更具活动id获取奖品信息

### 1、api描述
 更具活动id获取奖品信息

### 2、api参数

- 请求路径：api/v20190319/tribeActivityPrize/getTribeActivityPrizeInfoByActivityId
- Method: post


### 3、输入参数

参数名称|是否必选|数据类型|描述
-|-|-|-
activityId|Y|String|活动id
### 4、返回参数
参数名称|是否必选|数据类型|描述
-|-|-|-
tribeActivityPrizeId|Y|String|主键
tribeActivityPrizeName|Y|String|奖品名称
tribeActivityPrizeDesc|Y|String| 奖品内容描述
tribeActivityPrizeImage|Y|long| 奖品图片
prizeType|Y|long|奖品类型
prizeInnerId|Y|String|商品id
tribeId|Y|String|部落id
tribeName|Y|String|部落名称
activityId|Y|String|活动id
activityName|Y|String|活动名称
prizeCount|Y|int|奖品数量
```
{
	"activityId":2
}
```

### 6、返回示例

```json
{
    "code": "200",
    "msg": "成功",
    "result": [
        {
            "tribeActivityPrizeId": "1",
            "tribeActivityPrizeName": "上海迪士尼40元代金券",
            "tribeActivityPrizeDesc": "上海迪士尼40元代金券",
            "tribeActivityPrizeImage": "http://ndfs3.niwodai.net/ndfs3/M07/6F/F2/rBMMFlx96t-AXKa9AADD9lx-q7U661.jpg",
            "prizeType": "3",
            "prizeInnerId": "400001",
            "tribeId": "105",
            "tribeName": "八卦传送站",
            "activityId": "2",
            "activityName": "# 参与评论赢上海迪士尼门票40元抵扣券！",
            "prizeCount": "50",
            "status": "0",
            "createUser": "import by yuanye 20190305",
            "createDate": "2019-03-05 12:58:14",
            "updateUser": "update by yuanye 20190305",
            "updateDate": "2019-03-05 12:58:24",
            "delFlag": 0,
            "deleteFlag": 0,
            "deleteUser": null
        }
    ]
}
```