# 投票

## 一、用户互相伤害拉黑接口

### 1、api描述
你拉黑我，我拉黑你

### 2、api参数

- 请求路径： /api/user/back
- Method: POST

### 3、输入参数

参数名称|是否必选|数据类型|描述
-|-|-|-
sourceUserId|Y|string|拉黑发起人
targetUserId|Y|string|拉黑受害者


### 4、返回参数
参数名称|是否必选|数据类型|描述
-|-|-|-
code|Y|string|成功码
msg|Y|string|成功提示



### 5、请求示例

```json
{
 "sourceUserId":"101",
 "targetUserId":"102"
}
```

### 6、返回示例

```json
{
    "code": "200",
    "msg": "操作成功",
    "result": null
}
```


## 二、微博举报

### 1、api描述
净化网络环境、扫黄除黑

### 2、api参数

- 请求路径： /api/microblog/report
- Method: POST

### 3、输入参数

参数名称|是否必选|数据类型|描述
-|-|-|-
mid|Y|string|用户编号
microblogId|Y|string|微博Id
reason|Y|string|举报原因



### 4、返回参数
参数名称|是否必选|数据类型|描述
-|-|-|-
code|Y|string|成功码
msg|Y|string|成功提示


### 5、请求示例

```json
{
 "mid":"101",
 "microblogId":"13",
 "reason":"嘿咻嘿咻"
}
```

### 6、返回示例

```json
{
    "code": "200",
    "msg": "操作成功",
    "result": null
}
```