# 登录/注册

## 一、注册

### 1、api描述
用户注册

### 2、api参数

- 请求路径： /app/user/doRegister
- Method: post

### 3、输入参数

参数名称|是否必选|数据类型|描述
-|-|-|-
mobile|Y|string|手机号
verify|Y|string|短信验证码
recommendCode|N|string|推荐码

### 4、返回参数
参数名称|是否必选|数据类型|描述
-|-|-|-
mid|Y|string|用户id
bizType|Y|string|业务类型 //0:注册;1:登录
encryptKey|Y|string|根据mid生成的加密key

### 5、请求示例

```
curl 
```

### 6、返回示例

```json
{
    "code": "200",
    "msg": "成功",
    "result": {
        "bizType": 0,
        "mid": 2019000096840000,
        "encryptKey": "3Da6E97soAPXV7Ww"
    }
}
```

## 二、登录

### 1、api描述
用户登录

### 2、api参数

- 请求路径： /app/user/doLogin
- Method: post

### 3、输入参数

参数名称|是否必选|数据类型|描述
-|-|-|-
phone|Y|string|手机号
smsCode|Y|string|:短信验证码

### 4、返回参数
参数名称|是否必选|数据类型|描述
-|-|-|-
mid|Y|string|用户会员ID
encryptKey|N|string|根据mid生成的加密key

### 5、请求示例

```
curl 
```

### 6、返回示例

```json
{
    "code": "200",
    "msg": "成功",
    "result": {
        "mid": 2019000096838000,
        "encryptKey": "nx63LQIOqRuT08c5"
    }
}
```

## 三、获取图形验证码

### 1、api描述
获取图形验证码

### 2、api参数

- 请求路径： /app/user/imgCode
- Method: post

### 3、输入参数

参数名称|是否必选|数据类型|描述
-|-|-|-
code|Y|string|唯一标示的一串可以是uuid

### 4、返回参数
参数名称|是否必选|数据类型|描述
-|-|-|-
图片IO流


### 5、请求示例

```
curl 
```

### 6、返回示例

```json

```


## 四、获取短信验证码

### 1、api描述
获取短信验证码

### 2、api参数

- 请求路径： /app/user/sendMessage
- Method: post

### 3、输入参数

参数名称|是否必选|数据类型|描述
-|-|-|-
phone|Y|string|手机号码
smsType|N|string|0,"注册"/1,"登录"
imgCode|Y|string|图形验证码
code|Y|string|唯一标示的一串可以是uuid(和图像验证码一起传)


### 4、返回参数
参数名称|是否必选|数据类型|描述
-|-|-|-


### 5、请求示例

```
curl 
```

### 6、返回示例

```json
{
    "code": "200",
    "msg": "成功",
    "result": "success"
}
```